package main

import (
	"fmt"
	"image/color"
	"log"
	"math/rand"
	"time"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

const (
	MaxX = 320
	MaxY = 240
)

type GameData struct {
	Buf1    [MaxX][MaxY]int
	Buf2    [MaxX][MaxY]int
	Current int
}

var GD GameData
var lasttps float64

func fix(v, m int) int {
	if v < 0 {
		v = v + m
	}
	if v >= m {
		v = v - m
	}
	return v
}

func Conway() {

	//var Buf2 [MaxX][MaxY]int

	for x := 0; x < MaxX; x++ {
		for y := 0; y < MaxY; y++ {
			count := 0
			if GD.Current == 0 {
				if GD.Buf1[x][fix(y-1, MaxY)] != 0 {
					count++
				}
				if GD.Buf1[fix(x+1, MaxX)][fix(y-1, MaxY)] != 0 {
					count++
				}
				if GD.Buf1[fix(x+1, MaxX)][y] != 0 {
					count++
				}
				if GD.Buf1[fix(x+1, MaxX)][fix(y+1, MaxY)] != 0 {
					count++
				}
				if GD.Buf1[x][fix(y+1, MaxY)] != 0 {
					count++
				}
				if GD.Buf1[fix(x-1, MaxX)][fix(y+1, MaxY)] != 0 {
					count++
				}
				if GD.Buf1[fix(x-1, MaxX)][y] != 0 {
					count++
				}
				if GD.Buf1[fix(x-1, MaxX)][fix(y-1, MaxY)] != 0 {
					count++
				}

				if GD.Buf1[x][y] != 0 {
					if count < 2 {
						GD.Buf2[x][y] = 0
					}
					if count == 2 {
						GD.Buf2[x][y] = 1
					}
					if count == 3 {
						GD.Buf2[x][y] = 1
					}
					if count > 3 {
						GD.Buf2[x][y] = 0
					}
				}

				if GD.Buf1[x][y] == 0 {
					if count < 2 {
						GD.Buf2[x][y] = 0
					}
					if count == 2 {
						GD.Buf2[x][y] = 0
					}
					if count == 3 {
						GD.Buf2[x][y] = 1
					}
					if count > 3 {
						GD.Buf2[x][y] = 0
					}
				}
			} else {
				if GD.Buf2[x][fix(y-1, MaxY)] != 0 {
					count++
				}
				if GD.Buf2[fix(x+1, MaxX)][fix(y-1, MaxY)] != 0 {
					count++
				}
				if GD.Buf2[fix(x+1, MaxX)][y] != 0 {
					count++
				}
				if GD.Buf2[fix(x+1, MaxX)][fix(y+1, MaxY)] != 0 {
					count++
				}
				if GD.Buf2[x][fix(y+1, MaxY)] != 0 {
					count++
				}
				if GD.Buf2[fix(x-1, MaxX)][fix(y+1, MaxY)] != 0 {
					count++
				}
				if GD.Buf2[fix(x-1, MaxX)][y] != 0 {
					count++
				}
				if GD.Buf2[fix(x-1, MaxX)][fix(y-1, MaxY)] != 0 {
					count++
				}

				if GD.Buf2[x][y] != 0 {
					if count < 2 {
						GD.Buf1[x][y] = 0
					}
					if count == 2 {
						GD.Buf1[x][y] = 1
					}
					if count == 3 {
						GD.Buf1[x][y] = 1
					}
					if count > 3 {
						GD.Buf1[x][y] = 0
					}
				}

				if GD.Buf2[x][y] == 0 {
					if count < 2 {
						GD.Buf1[x][y] = 0
					}
					if count == 2 {
						GD.Buf1[x][y] = 0
					}
					if count == 3 {
						GD.Buf1[x][y] = 1
					}
					if count > 3 {
						GD.Buf1[x][y] = 0
					}
				}
			}
		}
	}
	/*
		for x := 0; x < MaxX; x++ {
			for y := 0; y < MaxY; y++ {
				GD.Buf1[x][y] = GD.Buf2[x][y]
				GD.Buf2[x][y] = 0
			}
		}
	*/
	if GD.Current == 0 {
		//GD.Buf1 = GD.Buf2
		GD.Current = 1
	} else {
		//GD.Buf2 = GD.Buf1
		GD.Current = 0
	}

	//copy(GD.Buf1, Buf2)
	//fmt.Println(tc)
}

// update is called every frame (1/60 [s]).
func update(screen *ebiten.Image) error {

	if ebiten.IsDrawingSkipped() {
		// When the game is running slowly, the rendering result
		// will not be adopted.
		return nil
	}

	//screen.Fill(color.Black)

	// Write your game's rendering.
	Conway()

	Img1, _ := ebiten.NewImage(MaxX, MaxY, ebiten.FilterDefault)
	for x := 0; x < MaxX; x++ {
		for y := 0; y < MaxY; y++ {
			if GD.Current == 0 {
				if GD.Buf1[x][y] != 0 {
					Img1.Set(x, y, color.White)
				}
			} else {
				if GD.Buf2[x][y] != 0 {
					Img1.Set(x, y, color.White)
				}
			}
		}
	}

	//goober := ebiten.CurrentTPS()
	msg := fmt.Sprintf("TPS: %0.2f", ebiten.CurrentTPS())
	ebitenutil.DebugPrint(screen, msg)

	// Write your game's logical update.
	screen.DrawImage(Img1, &ebiten.DrawImageOptions{})

	return nil
}

func main() {
	// Call ebiten.Run to start your game loop.
	GD.Current = 0
	rand.Seed(time.Now().UnixNano())
	for x := 0; x < MaxX; x++ {
		for y := 0; y < MaxY; y++ {
			if rand.Intn(10) == 1 {
				GD.Buf1[x][y] = 1
			}
		}
	}

	ebiten.SetMaxTPS(20)
	if err := ebiten.Run(update, MaxX, MaxY, 2, "Conway's Game of Life"); err != nil {
		log.Fatal(err)
	}
}
